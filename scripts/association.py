import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.cluster import DBSCAN

def catch(df, objectives):
    data = pd.DataFrame([])
    for item in objectives:
        key = '"amenity"=>"{}"'.format( item )

        specific_amenity = df[ df['other_tags'].str.contains(key) ]

        data = pd.concat([data, specific_amenity], axis = 0)
    
    return data

data = pd.read_csv('./test_stuff.csv', header = 0)

data['other_tags'] = data['other_tags'].fillna( 'NaN' )
data['name'] = data['name'].fillna( 'NaN' )

# Get only the relevant tagged & named points
amenity_data = data[data['other_tags'].str.contains('amenity')]

# Filter out the only relevant component, amenity type
#amenity_data['other_tags'] = amenity_data['other_tags'].str.replace('"addr:[a-z,A-Z].*"=>"[a-z,A-Z,0-9].*"','')
#amenity_data['other_tags'] = amenity_data['other_tags'].str.replace('"food"=>"[a-z,A-Z].*"','')
#amenity_data['other_tags'] = amenity_data['other_tags'].str.replace('"brand"=>"[a-z,A-Z].*"','')
#amenity_data['other_tags'] = amenity_data['other_tags'].str.replace('"gnis:[a-z,A-Z,0-9].*"=>"[a-z,A-Z,0-9].*"','')
#amenity_data['other_tags'] = amenity_data['other_tags'].str.replace('"denomination"=>"[a-z,A-Z,0-9].*"','')
#amenity_data['other_tags'] = amenity_data['other_tags'].str.replace('"internet_access"=>"[a-z,A-Z,0-9].*"','')
#amenity_data['other_tags'] = amenity_data['other_tags'].str.replace('"cuisine"=>"[a-z,A-Z,0-9,;].*"','')
#amenity_data['other_tags'] = amenity_data['other_tags'].str.replace('"outdoor_seating"=>"[a-z,A-Z,0-9,;].*"','')
#amenity_data['other_tags'] = amenity_data['other_tags'].str.replace('"website"=>"[a-z,A-Z,0-9,;,:,/,-].*"','')
#amenity_data['other_tags'] = amenity_data['other_tags'].str.replace('"contact:website"=>"[a-z,A-Z,0-9,;,:,/,-,.].*"','')

amenities = catch(amenity_data, ['restaurant','bar','pub','fast_food','cafe','bbq','biergarten','food_court','ice_cream'])

amenity_coords = amenities[['X','Y']].values

db = DBSCAN(eps = 0.005, min_samples = 2).fit(amenities[['X','Y']])
core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
core_samples_mask[db.core_sample_indices_] = True
labels = db.labels_

print(len(set(labels)) - (1 if -1 in labels else 0))

unique_labels = set(labels)
colors = [plt.cm.Spectral(each) for each in np.linspace(0,1, len(unique_labels))]
i = 0
for k, col in zip(unique_labels, colors):
    if k == -1:
        col = [0,0,0,1]
    
    class_member_mask = (labels == k)

    X = amenity_coords[class_member_mask][:,0]
    Y = amenity_coords[class_member_mask][:,1]
    
    plt.scatter( X, Y, color = col )
    
    class_member_mask = (labels == k)

    i += 1
plt.title('Estimated clusters.')
plt.show()