import pandas as pd
import numpy as np
from shapely import wkt
import geopandas as gpd
from geopandas import GeoDataFrame
import matplotlib.pyplot as plt

df = pd.read_csv('./trajclean-coord-destination_guessed.csv')

guessed_destination = df.groupby('location_best_guess')['trip_id'].nunique().sort_values(ascending = False).reset_index(name = 'count')
guessed_destination.columns = ['name','hits']

top_destinations = guessed_destination[0:25]['name']
print(top_destinations)

for trajectory_id in top_destinations:
    trajectories = df[df['location_best_guess'] == trajectory_id]
    trajectories.to_csv('./most_important_destinations/{}.csv'.format( trajectory_id.replace(' ','_').replace('\'','') ), index = False)