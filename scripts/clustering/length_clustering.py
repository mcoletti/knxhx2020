import pandas as pd
import numpy as np
from sklearn.cluster import KMeans
from sklearn.preprocessing import StandardScaler, MinMaxScaler
import matplotlib.pyplot as plt
import seaborn as sns
#from scipy.spatial.distance.dist import cdist

df = pd.read_csv('../knxhx2020/data/trajdims.csv', header = 0)

#fit_data = df[['length_m','vcount','movedist_m','len2dist','rotund']]
fit_data = df[['length_m']]

print(fit_data)

scaler = MinMaxScaler()
fit_data = scaler.fit_transform(fit_data)
print(fit_data)

'''
inertia_series = []
for k in range(1,25):
    model = KMeans(n_clusters = k).fit(fit_data)
    inertia_series.append(model.inertia_)
    print(k)

plt.plot(inertia_series)
plt.show()
'''
model = KMeans(n_clusters = 5).fit(fit_data)
labels = model.labels_
df['label'] = labels

sorted_df = df.sort_values(by = 'length_m')
sorted_lengths = sorted_df['length_m'].values
sorted_labels = sorted_df['label'].values

print(len(set(labels)) - (1 if -1 in labels else 0))

unique_labels = set(labels)
colors = [plt.cm.plasma(each) for each in np.linspace(0,1, len(unique_labels))]
col = [ colors[i] for i in sorted_labels]
plt.scatter(range(len(sorted_lengths)), sorted_lengths, color = col, s = 4)

plt.title('Estimated clusters')
plt.legend()
plt.show()

#plt.scatter(fit_data['numeric_month'].values, fit_data['numeric_weekday'].values)

#ax = plt.axes(projection = '3d')
#ax.scatter3D(fit_data['numeric_month'], fit_data['numeric_weekday'], fit_data['numeric_time'])