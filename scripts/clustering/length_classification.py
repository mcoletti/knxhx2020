import pandas as pd
import numpy as np
from sklearn.cluster import KMeans
from sklearn.preprocessing import StandardScaler, MinMaxScaler
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.tree import DecisionTreeClassifier
from sklearn import tree
#from scipy.spatial.distance.dist import cdist

df = pd.read_csv('../knxhx2020/data/trajdims.csv', header = 0)

#fit_data = df[['length_m','vcount','movedist_m','len2dist','rotund']]
cluster_data = df[['length_m']]

scaler = MinMaxScaler()
cluster_data = scaler.fit_transform(cluster_data)

model = KMeans(n_clusters = 5).fit(cluster_data)
labels = model.labels_
df['label'] = labels

fit_data = df[['start_lat','start_lon','dow','months','dayhr']]

fit_data = scaler.fit_transform(fit_data)

clf = DecisionTreeClassifier(max_depth = 5)
clf = clf.fit(fit_data, df['label'].values)
tree.plot_tree(clf, max_depth = 5, fontsize = 5)
plt.show()