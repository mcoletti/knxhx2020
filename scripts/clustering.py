import pandas as pd
import numpy as np
from sklearn.cluster import KMeans
from sklearn.preprocessing import StandardScaler, MinMaxScaler
import matplotlib.pyplot as plt
#from scipy.spatial.distance.dist import cdist

df = pd.read_csv('./knxhx2020/data/trajclean-coord.csv', header = 0)

df = df.drop(columns = ['start','end','traj'])

fit_data = df[['start_lon','end_lon','start_lat','end_lat','length_m']]
#fit_data['numeric_month'] = pd.to_datetime(df['trip_date']).dt.month
fit_data['numeric_weekday'] = pd.to_datetime(df['trip_date']).dt.weekday
#fit_data['numeric_time'] = pd.to_datetime(df['trip_time']).dt.second + pd.to_datetime(df['trip_time']).dt.minute * 60 + pd.to_datetime(df['trip_time']).dt.hour * 60 * 60

fit_data = fit_data[fit_data['numeric_weekday'] == 0]

print(fit_data)

scaler = MinMaxScaler()
fit_data = scaler.fit_transform(fit_data)
print(fit_data)

inertia_series = []
for k in range(1,25):
    model = KMeans(n_clusters = k).fit(fit_data)
    inertia_series.append(model.inertia_)
    print(k)

plt.plot(inertia_series)

#plt.scatter(fit_data['numeric_month'].values, fit_data['numeric_weekday'].values)

#ax = plt.axes(projection = '3d')
#ax.scatter3D(fit_data['numeric_month'], fit_data['numeric_weekday'], fit_data['numeric_time'])
plt.show()