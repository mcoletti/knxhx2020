import pandas as pd
import numpy as np
from shapely import wkt
import geopandas as gpd
from geopandas import GeoDataFrame
import matplotlib.pyplot as plt

df = pd.read_csv('./knxhx2020/data/trajclean-coord.csv')
df2 = pd.read_csv('./test_stuff.csv')
df2['name'] = df2['name'].fillna( 'NaN' )
df2 = df2[df2['name'] != 'NaN']
df2 = df2[df2['place'] != 'hamlet']
df2 = df2[df2['place'] != 'county']

end_coords = df[['start_lon','start_lat']].values
possible_coords = df2[['X','Y','name']].values

end_locations = []
for i in range(len(end_coords)):
    end_locations.append((end_coords[i,0], end_coords[i,1]))

possible_locations = []
for i in range(len(possible_coords)):
    possible_locations.append((possible_coords[i,0], possible_coords[i,1], possible_coords[i,2]))

location_estimations = []
lon_estimations = []
lat_estimations = []
i = 0
for end in end_locations:
    smallest_distance = 100.0
    for possible in possible_locations:
        distance = np.sqrt( (possible[0] - end[0])**2  + (possible[1] - end[1])**2)
        if (distance < smallest_distance):
            smallest_distance = distance
            estimated_location = possible
    location_estimations.append(estimated_location[2])
    lon_estimations.append(estimated_location[0])
    lat_estimations.append(estimated_location[1])
    print(i)
    i += 1
print(len(lon_estimations))
print(len(lat_estimations))
identified_df = df
identified_df['origin_best_guess'] = location_estimations
identified_df['origin_lon'] = lon_estimations
identified_df['origin_lat'] = lat_estimations

# Guess best destinations
end_coords = df[['end_lon','end_lat']].values
possible_coords = df2[['X','Y','name']].values

end_locations = []
for i in range(len(end_coords)):
    end_locations.append((end_coords[i,0], end_coords[i,1]))

possible_locations = []
for i in range(len(possible_coords)):
    possible_locations.append((possible_coords[i,0], possible_coords[i,1], possible_coords[i,2]))

location_estimations = []
lon_estimations = []
lat_estimations = []
i = 0
for end in end_locations:
    smallest_distance = 100.0
    for possible in possible_locations:
        distance = np.sqrt( (possible[0] - end[0])**2  + (possible[1] - end[1])**2)
        if (distance < smallest_distance):
            smallest_distance = distance
            estimated_location = possible
    location_estimations.append(estimated_location[2])
    lon_estimations.append(estimated_location[0])
    lat_estimations.append(estimated_location[1])
    print(i)
    i += 1

identified_df['destination_best_guess'] = location_estimations
identified_df['destination_lon'] = lon_estimations
identified_df['destination_lat'] = lat_estimations

identified_df.to_csv('trajclean-coord-origin_and_destinations_guessed.csv', index = False)
