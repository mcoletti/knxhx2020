import pandas as pd
import numpy as np
from shapely import wkt
import geopandas as gpd
from geopandas import GeoDataFrame
import matplotlib.pyplot as plt

df = pd.read_csv('./trajclean-coord-origin_and_destinations_guessed.csv')
df2 = pd.read_csv('./test_stuff.csv')
df2['name'] = df2['name'].fillna( 'NaN' )
df2 = df2[df2['name'] != 'NaN']
df2 = df2[df2['place'] != 'hamlet']
df2 = df2[df2['place'] != 'county']

sorted = df.groupby('destination_best_guess')['trip_id'].nunique().sort_values(ascending = False).reset_index().head(n = 20).values
cluster_coords = []
for item in sorted:
    print(item[0])
    data = df2[df2['name'] == item[0]][['X','Y']]
    print(data)
    cluster_coords.extend( (data[0], data[1], item[1] ) )
print(cluster_coords)
cluster_coords = pd.DataFrame(cluster_coords)
print(cluster_coords)