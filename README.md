# KNXHX 2020

## Goals

* Shared Micromobility and Infrastructure
  * Shed light on operations, management, safety, Sustainability
* Trip Labeling
  * Utilitarian
  * Recreational
* Parts of the city do riders most frequently
* Infrastructure improvement

## Software Architecture

* General considerations
  * Open Source
  * Cloud-based
* Backend
  * PostgreSQL with PostGIS latest versions
  * Standard docker image, can be run in the cloud or on the workstation
* Frontend
  * Jupyter
  * Pandas
  * Geopandas -- Can be used with git repos or Google Collab
  * QGIS
* ML Methods
  * Density-based spatial clustering of applications with noise (DBSCAN)
  * K means clustering
  * Neural networks for prediction (future)
