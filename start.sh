#
# command to start services
#

docker run --rm --name knxhx2020 -e POSTGRES_PASSWORD=knxhx -e PGDATA=/data -v $PWD/db:/data -p 5433:5432 -d postgis/postgis

docker exec -it -u postgres knxhx2020 psql

psql postgresql://scooter:scooter@localhost:5433/scooter

conda install jupyter

pip install -r requirements.txt

jupyter notebook